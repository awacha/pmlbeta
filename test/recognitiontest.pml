delete all
# build a test peptide
fab KWKLFKKIGAVLKVL, cm15, ss=1
# destroy names
alter all, name=elem+str(index)
import pymol.plugins
pymol.plugins.plugins['pmlbeta'].module.recognition.residue_topology_candidates('cm15', list(range(1,16)), '/home/wachaandras/gromacs/betapeptides/ffcompare/simulation/forcefields/charmm-beta.ff/merged.rtp')