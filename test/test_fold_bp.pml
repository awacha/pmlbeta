### Demonstrate the post-build folding capabilities of pmlbeta

# create a dummy beta-peptide in the default straight conformation
betafab2 obj, (S)B3hV, (S)B3hV, (S)B3hV, (S)B3hV, (S)B3hV, (S)B3hV, (S)B3hV

# copy the previous object and fold it the simple way
create obj_h14msingle, obj
fold_bp H14M, obj_h14msingle

# copy the original object and fold it by specifying the same fold for every residue
create obj_h14mall, obj
fold_bp [ H14M H14M H14M H14M H14M H14M H14M ], obj_h14mall

# now specify different folds for each residue
create obj_mixed, obj
fold_bp [ H14M H14M (180 180 180) H14M H14M (180 180 180) (180 180 180) ], obj_mixed

# create a simple alpha-peptide
betafab alpha, (S)AV, (S)AV, (S)AV, (S)AV, (S)AV, (S)AV

# fold the alpha-peptide into an alpha-helix conformation
create alpha_single, alpha
fold_bp Alpha-helix, alpha_single

# some coloring
color green, e. C

# center all six molecules
cmd.translate([-coord for coord in cmd.centerofmass('obj')], 'obj', camera=0)
cmd.translate([-coord for coord in cmd.centerofmass('obj_h14msingle')], 'obj_h14msingle', camera=0)
cmd.translate([-coord for coord in cmd.centerofmass('obj_h14mall')], 'obj_h14mall', camera=0)
cmd.translate([-coord for coord in cmd.centerofmass('obj_mixed')], 'obj_mixed', camera=0)
cmd.translate([-coord for coord in cmd.centerofmass('alpha')], 'alpha', camera=0)
cmd.translate([-coord for coord in cmd.centerofmass('alpha_single')], 'alpha_single', camera=0)
center

# adjust the window size
viewport 800, 600

# grid mode
set grid_mode, 1
set grid_slot, 1, obj
set grid_slot, 2, obj_h14msingle
set grid_slot, 3, obj_h14mall
set grid_slot, 4, obj_mixed
set grid_slot, 5, alpha
set grid_slot, 6, alpha_single

set_view (\
     0.854473352,    0.429739147,    0.291887671,\
    -0.330565572,    0.883216739,   -0.332641095,\
    -0.400748819,    0.187747195,    0.896745861,\
    -0.000000000,    0.000000000,  -95.384521484,\
    -0.138860703,    0.010619164,    0.016929626,\
    58.306636810,  132.462371826,  -20.000000000 )

ray
png test_fold_bp.png, width=10cm, dpi=600, ray=1
save test_fold_bp.pse