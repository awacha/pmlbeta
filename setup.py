#!/usb/bin/env python
import os

from setuptools import setup, find_packages

import compileui
compileui.run()

setup(name='pmlbeta', author='Andras Wacha',
      author_email='awacha@gmail.com', url='http://gitlab.com/awacha/pmlbeta',
      description='PyMOL plugin for beta peptides',
      package_dir={'':'src'},
      packages=find_packages('src'),
      setup_requires=['setuptools_scm'],
      install_requires=['pymol', 'networkx'],
      use_scm_version=True,
      license="BSD 3-clause",
      zip_safe=True,
      )
