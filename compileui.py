from PyQt5 import uic
import os

def run():
    rcc_output = os.path.join('src', 'pmlbeta', 'betafabgui2', 'icons_rc.py')
    rcc_input = os.path.join('src', 'pmlbeta', 'betafabgui2', 'icons', 'icons.qrc')
    os.system('pyrcc5 {} -o {}'.format(rcc_input, rcc_output))

    def compile_uis(packageroot):
        if uic.compileUi is None:
            return
        for dirpath, dirnames, filenames in os.walk(packageroot):
            for fn in [fn_ for fn_ in filenames if fn_.endswith('.ui')]:
                fname = os.path.join(dirpath, fn)
                pyfilename = os.path.splitext(fname)[0] + '_ui.py'
                with open(pyfilename, 'wt', encoding='utf-8') as pyfile:
                    uic.compileUi(fname, pyfile, from_imports=True, import_from='.')
                print('Compiled UI file: {} -> {}.'.format(fname, pyfilename))

    compile_uis(os.path.join('src', 'pmlbeta'))


if __name__ == '__main__':
    run()
