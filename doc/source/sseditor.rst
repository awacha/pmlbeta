.. _sseditor_gui:

Secondary structure editor
==========================

`pmlbeta` provides a graphical utility for editing the secondary structure database. It can be reached via the Plugin
menu of PyMOL

.. figure:: _static/sseditor.png

    The secondary structure dialog

The dialog is a simple list of secondary structures, with the possibility to add, duplicate and remove entries. Each
entry can be edited by double-clicking on the cells in the table. Changes are updated only if the `OK` or `Apply` button
is pressed. The default values can be restored by the `Add defaults` button on the toolbar.

Changes to the secondary structure database are saved for the next PyMOL session.
