Installation
============

Requirements
------------

`pmlbeta` is an extension for the `PyMOL molecular graphics system <https://www.pymol.org>`_.
Only version 2.0 and above is supported.
The additional dependencies are:

Python:
    at least version 3.0 is needed, 3.5 and above is tested. Python 2 is not supported.

PyQt5:
    for the graphical user interface

PyMOL:
    testing is done with the most up-to-date open source version, currently 2.3.0. Other versions above 2.0
    should also work, but not guaranteed. However, if you find compatibility issues, :ref:`please file a bug report <bugreport>`.


Installing from a pre-built package
-----------------------------------

Download the latest binary package from
https://gitlab.com/awacha/pmlbeta/raw/binaries/pmlbeta_latest.zip
and install it with the plugin manager of PyMOL.

Please note that PyMOL (at least up to the open source version 2.3.0) only supports installing ZIP plugins by manually
downloading them and selecting the downloaded file. Giving the above URL to the package manager does not work.


Manual installation
-------------------

Check out the git repository:

.. code:: bash

    $ git clone https://gitlab.com/awacha/pmlbeta.git


Create the plugin zip file with:

.. code:: bash

    $ python makebundle.py

Then use the plugin manager of PyMOL to install the plugin from the
just created zip file.



