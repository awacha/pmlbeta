import compileui
import zipfile
import subprocess
import os

version=subprocess.check_output(['git', 'describe', '--tags']).decode('utf-8').strip()

compileui.run()

with zipfile.ZipFile('pmlbeta_{}.zip'.format(version), 'w', compression=zipfile.ZIP_DEFLATED) as zf:
    rootdir=os.path.join('src', 'pmlbeta')
    for folder, subdirs, files in os.walk(rootdir):
        if folder.endswith('__pycache__'):
            continue
        for f in files:
            if f.endswith('~') or f.endswith('.pyc'):
                continue
            filename=os.path.join(folder, f)
            arcname = filename.replace(rootdir, '')
            while arcname[0] in [os.path.sep, os.path.altsep]:
                arcname=arcname[1:]
            arcname=os.path.join('pmlbeta', arcname)
            zf.write(filename, arcname)
print('pmlbeta_{}.zip created.'.format(version))