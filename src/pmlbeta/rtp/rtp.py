from typing import Iterable
import warnings
try:
    from networkx import Graph
    from networkx.algorithms.isomorphism import GraphMatcher
except:
    Graph = None
    GraphMatcher = None
    warnings.warn('NetworkX not installed. Please install it to be able to use the structure recognition facilities!')
from pymol import cmd

from .atom import RTPAtom
from .bondedtypes import BondedTypes
from .interaction import Interaction


class RTPEntry:
    """Represents a residue from a GROMACS residue topology (.rtp) file"""

    def __init__(self, name: str, bondedtypes: BondedTypes):
        """

        :param name: residue name
        :type name: str
        :param bondedtypes: information from the [ bondedtypes ] block
        :type bondedtypes: bondedtypes instance
        """
        self.atoms = []
        self.bonds = []
        self.impropers = []
        self.cmaps = []
        self.startatom = None
        self.endatom = None
        self.name = name
        self.bondedtypes = bondedtypes

    @classmethod
    def loadrtpfile(cls, filename: str) -> Iterable["RTPEntry"]:
        """Load all residue topology entries from a GROMACS residue topology database.

        This is a generator function, `yield`-ing the residue topology entries.

        :param filename: RTP file name
        :type filename: str
        :return: yields residue topology entries
        :rtype: RTPEntry
        """
        with open(filename, 'rt') as f:
            rtp = None
            bondedtypes = None
            blockname = None
            for line in f:
                strippedline = line.split(';')[0].strip()
                if not strippedline:
                    continue
                elif strippedline.startswith('[') and strippedline.endswith(']'):
                    blockname = strippedline[1:-1].strip()
                    if blockname == 'cmap':
                        blockname = 'cmaps'  # to conform all the other cases
                    if blockname not in ['bondedtypes', 'atoms', 'bonds', 'dihedrals', 'impropers', 'angles',
                                         'exclusions', 'cmaps']:
                        # this is not a special block name, must be a new residue.
                        if rtp is not None:
                            yield rtp
                        rtp = RTPEntry(blockname, bondedtypes)
                elif blockname == 'bondedtypes':
                    bondedtypes = BondedTypes(*[int(x) for x in strippedline.split()])
                elif blockname == 'atoms':
                    name, atomtype, pcharge, cgroup = strippedline.split()
                    rtp.atoms.append(RTPAtom(name, atomtype, float(pcharge), int(cgroup)))
                elif blockname in ['bonds', 'dihedrals', 'angles', 'impropers', 'exclusions', 'cmaps']:
                    getattr(rtp, blockname).append(Interaction(blockname[:-1], *(strippedline.split())))
                else:
                    print(blockname)
                    print(line)
                    assert False  # we should never reach this line

    def _findjoiningatoms(self):
        """Find atoms by which this residue is joined to the previous and the next one."""
        self.startatom = None
        self.endatom = None
        for b in self.bonds:
            if b.atomnames[0].startswith('-'):
                self.startatom = [a for a in self.atoms if a.name == b.atomnames[1]][0]
            elif b.atomnames[1].startswith('-'):
                self.startatom = [a for a in self.atoms if a.name == b.atomnames[0]][0]
            elif b.atomnames[0].startswith('+'):
                self.endatom = [a for a in self.atoms if a.name == b.atomnames[1]][0]
            elif b.atomnames[1].startswith('+'):
                self.endatom = [a for a in self.atoms if a.name == b.atomnames[0]][0]

    def toGraph(self) -> Graph:
        """Representation of this residue topology entry in a NetworkX undirected graph

        :return: graph representation of this RTP entry
        :rtype: NetworkX Graph
        """
        if Graph is None:
            raise RuntimeError('NetworkX is not installed, structure recognition does not work.')
        graph = Graph()
        for atom in self.atoms:
            graph.add_node(atom.name, element=atom.name[0],  # ToDo: better element assignment
                           isstart=self.startatom == atom, isend=self.endatom == atom)
        for bond in self.bonds:
            if any([atomname.startswith('-') or atomname.startswith('+') for atomname in bond.atomnames]):
                continue  # do not add residue-connecting bonds
            graph.add_edge(bond.atomnames[0], bond.atomnames[1])
        return graph

    def __getitem__(self, item:str) -> RTPAtom:
        """Shorthand to get the atom with the given name.

        :param item: atom name
        :type item: str
        :return: atom object
        :rtype: RTPAtom
        """
        return [a for a in self.atoms if a.name == item][0]

    def matchResidue(self, molecule: str, resi: int, updateifisomorphic: bool = False) -> bool:
        """Match a portion of a molecule with this residue topology

        :param molecule: PyMOL selection or molecule name of the molecule
        :type molecule: str
        :param resi: residue index
        :type resi: int
        :param updateifisomorphic: if True, update atom parameters if the topology matches the model residue.
        :type updateifisomorphic: bool
        :return: True if the topology matches the selected residue of the model, False otherwise
        :rtype: bool
        """
        if Graph is None:
            raise RuntimeError('NetworkX is not installed, structure recognition does not work.')
        # get a Python representation of the selection
        model = cmd.get_model(molecule)

        # initialize the NetworkX graph for the model
        graph = Graph()

        # first add nodes to the graph: each atom will be a node.
        for iatom, atom in enumerate(model.atom):
            if atom.resi_number != resi:
                # only add atoms from the selected residue.
                continue
            graph.add_node(
                iatom, isstart=False, isend=False, element=atom.symbol)  # start and end atoms will be marked later

        # add bonds as edges to the graph
        for ibond, bond in enumerate(model.bond):
            atom1 = model.atom[bond.index[0]]
            atom2 = model.atom[bond.index[1]]
            # first see starting and ending atoms
            if atom1.resi_number == resi - 1 and atom2.resi_number == resi:
                # atom2 is the starting atom
                graph.nodes[bond.index[1]]['isstart'] = True
            elif atom2.resi_number == resi - 1 and atom1.resi_number == resi:
                # atom1 is the starting atom
                graph.nodes[bond.index[0]]['isstart'] = True
            if atom1.resi_number == resi and atom2.resi_number == resi + 1:
                # atom1 is the ending atom
                graph.nodes[bond.index[0]]['isend'] = True
            elif atom2.resi_number == resi and atom1.resi_number == resi + 1:
                # atom2 is the ending atom
                graph.nodes[bond.index[1]]['isend'] = True
            # now add the bond but only if both atoms are in this residue
            if atom1.resi_number == resi and atom2.resi_number == resi:
                graph.add_edge(bond.index[0], bond.index[1])

        # Match the topologies by checking if the just constructed graph of the model and the graph representation of
        # this topology are isomorphic.
        matcher = GraphMatcher(
            graph, self.toGraph(),
            node_match=lambda n1, n2: (n1['element'] == n2['element']), # match atoms if the elements are the same.
        )
        if matcher.is_isomorphic() and updateifisomorphic:
            # if isomorphism found and update is requested, update the atom parameters.
            mapping = matcher.mapping
            for iatom, atom in enumerate(model.atom):
                if atom.resi_number != resi:
                    continue
                cmd.alter('({}) and (index {})'.format(molecule, atom.index), 'name="{}"'.format(mapping[iatom]))
                cmd.alter('({}) and (index {})'.format(molecule, atom.index),
                          'partial_charge="{}"'.format(self[mapping[iatom]].pcharge))
                cmd.alter('({}) and (index {})'.format(molecule, atom.index), 'resn="{}"'.format(self.name))

        # return True if this topology matched the model.
        return matcher.is_isomorphic()
